import { LightningElement } from 'lwc';

export default class HelloWorld3 extends LightningElement {
    greeting = 'World'
    changeHandler(event){
        this.greeting = event.target.value;
    }
}